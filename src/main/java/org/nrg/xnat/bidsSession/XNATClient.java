package org.nrg.xnat.bidsSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import org.nrg.xnat.bidsSession.datamodel.*;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class XNATClient {
    private final RestTemplate restTemplate;
    private final ObjectMapper om;

    public XNATClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.om = new ObjectMapper();
    }

    public ImageSessionData getSession( String sessionID) {
        HttpHeaders headers = new HttpHeaders();

        String auth = "dmaffitt" + ":" + "xxx";
        String authHeader = "Basic " + Base64.getEncoder().encodeToString( auth.getBytes(StandardCharsets.UTF_8));

        headers.set( "Authorization", authHeader );

        HttpEntity<String> entity = new HttpEntity<>("body", headers);
        String url = String.format("https://cnda-dev-maffi1.nrg.wustl.edu/data/experiments/%s", sessionID);

        final ResponseEntity<ImageSessionData> response = restTemplate.exchange(url, HttpMethod.GET, entity, ImageSessionData.class);
        return response.getBody();
    }

    public Map<String, String> getSiteBidsMap() throws XNATClientException {
        String url = String.format("https://cnda-dev-maffi1.nrg.wustl.edu/data/config/bids/bidsmap?format=json");
        return getBidsMap( url);
    }

    public Map<String, String> getProjectBidsMap( String projectLabel) {
        String url = String.format("https://cnda-dev-maffi1.nrg.wustl.edu/data/projects/%s/config/bids/bidsmap?format=json", projectLabel);
        return getBidsMap( url);
    }

    public Map<String, String> getBidsMap( String url) {
        HttpHeaders headers = new HttpHeaders();

        String auth = "dmaffitt" + ":" + "xxx";
        String authHeader = "Basic " + Base64.getEncoder().encodeToString( auth.getBytes(StandardCharsets.UTF_8));

        headers.set( "Authorization", authHeader );

        HttpEntity<String> entity = new HttpEntity<>("body", headers);

        final ResponseEntity<ConfigResponse> response = restTemplate.exchange(url, HttpMethod.GET, entity, ConfigResponse.class);
        if( response.getStatusCode().is2xxSuccessful()) {
            final List<ConfigEntry> configEntryList = response.getBody().getConfigEntryList();
            ConfigEntry configEntry = configEntryList.get(0);
            ObjectMapper om = new ObjectMapper();
            try {
                List<BidsMapEntry> bidsMapEntryList = om.readValue(configEntry.getContents(), new TypeReference<List<BidsMapEntry>>() {});
                return bidsMapEntryList.stream().collect(Collectors.toMap(BidsMapEntry::getSeriesDescription, BidsMapEntry::getBidsName, (v1, v2) -> v1));
            } catch (JsonProcessingException e) {
                throw new XNATClientException(e);
            }
        }
        else {
            throw new XNATClientException("Error getting BIDS map: " + response.getStatusCode());
        }
    }

}
