package org.nrg.xnat.bidsSession;

import org.nrg.xnat.bidsSession.datamodel.ImageScanData;

import java.nio.file.Path;

/**
 * BidsScanData: wrapper class of ImageScanData to bundle extra BIDS metadata associated with the scan.
 *
 * This would be better as an extension of ImageScanData but that requires extending the xsd. This is easier for now.
 */
public class BidsScanData {
    private ImageScanData imageScanData;
    private String seriesName;
    private String bidsName;
    private Path dicomDir;

    public BidsScanData( ImageScanData imageScanData) {
        this.imageScanData = imageScanData;
    }

    public ImageScanData getImageScanData() {
        return imageScanData;
    }

    public void setImageScanData(ImageScanData imageScanData) {
        this.imageScanData = imageScanData;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public String getBidsName() {
        return bidsName;
    }

    public void setBidsName(String bidsName) {
        this.bidsName = bidsName;
    }

    public Path getDicomDir() {
        return dicomDir;
    }

    public void setDicomDir(Path dicomDir) {
        this.dicomDir = dicomDir;
    }
}
