package org.nrg.xnat.bidsSession;

import org.nrg.xnat.bidsSession.datamodel.ImageSessionData;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class BidsSession {
    private Dcm2Niix dcm2Niix;
    private ScanIdentifier scanIdentifier;
    private BidsNamer bidsNamer;
    private XNATClient xnatClient;

    public BidsSession( ScanIdentifier scanIdentifier, BidsNamer bidsNamer, Dcm2Niix dcm2Niix, XNATClient xnatClient) {
        this.scanIdentifier = scanIdentifier;
        this.dcm2Niix = dcm2Niix;
        this.bidsNamer = bidsNamer;
        this.xnatClient = xnatClient;
    }

    public int process( String projectLabel, String subjectLabel, String sessionLabel, String sessionID, Path sessionDir, Path niftiDir, boolean overwrite) throws IOException, InterruptedException{

        System.out.println("Get imageSession: " + sessionID);
        ImageSessionData session = xnatClient.getSession( sessionID);

        System.out.println("imageSession: " + session.getLabel());

        List<BidsScanData> scans = scanIdentifier.findScans( session);
        if( scans.isEmpty()) {
            System.out.println("No qualifying scans found.");
        }

        bidsNamer.addBidsNames( projectLabel, subjectLabel, sessionLabel, scans);

        scans.forEach(System.out::println);

        scans.forEach( s -> s.setDicomDir( sessionDir.resolve( String.format( "SCANS/%s/DICOM", s.getImageScanData().getID()))));

        scans.forEach( System.out::println);

        for( BidsScanData scan: scans) {
            Path scanBidsDir = niftiDir.resolve( Paths.get(scan.getImageScanData().getID(), "NIFTI"));
            dcm2Niix.run( scan.getBidsName(), scanBidsDir, scan.getDicomDir());
        }

        return 0;
    }

}