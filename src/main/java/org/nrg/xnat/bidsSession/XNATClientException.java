package org.nrg.xnat.bidsSession;

public class XNATClientException extends RuntimeException {
    public XNATClientException( Exception e) {
        super(e);
    }
    public XNATClientException( String msg) {
        super(msg);
    }
}
