package org.nrg.xnat.bidsSession;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class ProcessRunner {
    private ProcessBuilder pb;

    public ProcessRunner( String... args) {
        pb = new ProcessBuilder( args);
    }

    public int run( Consumer<String> out, Consumer<String> err) throws IOException, InterruptedException {
        Process process = pb.start();

        StreamGobbler outGobbler = new StreamGobbler( process.getInputStream(), out);
        StreamGobbler errGobbler = new StreamGobbler( process.getErrorStream(), err);
        Executors.newSingleThreadExecutor().submit( outGobbler);
        Executors.newSingleThreadExecutor().submit( errGobbler);

        return process.waitFor();
    }

    /**
     * StreamGobbler copies an input stream to String consumer in its own thread to minimize blocking.
     */
    private static class StreamGobbler implements Runnable {
        private InputStream inputStream;
        private Consumer<String> consumer;

        public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
            this.inputStream = inputStream;
            this.consumer = consumer;
        }

        @Override
        public void run() {
            new BufferedReader(new InputStreamReader(inputStream)).lines()
                    .forEach(consumer);
        }
    }
}
