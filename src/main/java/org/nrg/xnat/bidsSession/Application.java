package org.nrg.xnat.bidsSession;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class Application {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {

        RestTemplate rt = builder.build();
        List<HttpMessageConverter<?>> messageConverters = rt.getMessageConverters();
        messageConverters.add( new MappingJackson2HttpMessageConverter());
        rt.setMessageConverters( messageConverters);
        return rt;
    }

    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.featuresToEnable(SerializationFeature.WRAP_ROOT_VALUE);
        builder.featuresToEnable(DeserializationFeature.UNWRAP_ROOT_VALUE);
        return builder;
    }

    @Bean XNATClient xnatClient( RestTemplate restTemplate) { return new XNATClient( restTemplate);}

    @Bean ScanIdentifier scanIdentifier() { return new AnatQCScanIdentifier();}

    @Bean BidsNamer bidsNamer( XNATClient xnatClient) { return new BidsNamer( xnatClient);}

    @Bean
    public CommandLineRunner run( ScanIdentifier scanIdentifier, BidsNamer bidsNamer, XNATClient xnatClient) throws Exception {
        return args -> {
            Path sessionDir = null;
            Path niftiDir = null;
            String sessionID = null;
            String sessionLabel = null;
            String subjectLabel = null;
            String projectLabel = null;
            boolean overwrite = false;

            System.out.println("Parsing command line: " + Arrays.stream(args).collect( Collectors.joining(" ")));

            int iarg = 0;
            while( iarg < args.length) {
                switch (args[iarg].toLowerCase()) {
                    case "--sessionid":
                        sessionID = nextArg( args, ++iarg);
                        if( sessionID == null) {
                            String msg = "Missing sessionID argument for flag --sessionID";
                            System.out.println(msg);
                            System.exit(1);
                        }
                        iarg++;
                        break;
                    case "--sessionlabel":
                        sessionLabel = nextArg( args, ++iarg);
                        if( sessionLabel == null) {
                            String msg = "Missing sessionLabel argument for flag --sessionLabel";
                            System.out.println(msg);
                            System.exit(1);
                        }
                        iarg++;
                        break;
                    case "--subjectlabel":
                        subjectLabel = nextArg( args, ++iarg);
                        if( subjectLabel == null) {
                            String msg = "Missing subjectLabel argument for flag --subjectLabel";
                            System.out.println(msg);
                            System.exit(1);
                        }
                        iarg++;
                        break;
                    case "--projectlabel":
                        projectLabel = nextArg( args, ++iarg);
                        if( projectLabel == null) {
                            String msg = "Missing projectLabel argument for flag --projectLabel";
                            System.out.println(msg);
                            System.exit(1);
                        }
                        iarg++;
                        break;
                    case "--overwrite":
                        String a = nextArg( args, ++iarg);
                        if( a == null) {
                            String msg = "Missing overwrite argument for flag --overwrite";
                            System.out.println(msg);
                            System.exit(1);
                        }
                        if( "true".equals( a.toLowerCase()) || "t".equals( a.toLowerCase()))
                            overwrite = true;
                        else if( "false".equals( a.toLowerCase()) || "f".equals( a.toLowerCase()))
                            overwrite = false;
                        else {
                            String msg = "Invalid boolean overwrite argument: " + a;
                            System.out.println(msg);
                            System.exit(1);
                        }
                        iarg++;
                        break;
                    case "--sessiondir":
                        String sessionDirString = nextArg( args, ++iarg);
                        if( sessionDirString == null) {
                            String msg = "Missing session root path argument for flag --sessionDir";
                            System.out.println(msg);
                            System.exit(1);
                        }
                        sessionDir = Paths.get(sessionDirString);
                        iarg++;
                        break;
                    case "--niftidir":
                        String niftiDirString = nextArg( args, ++iarg);
                        if( niftiDirString == null) {
                            String msg = "Missing nifti root path argument for flag -niftiDir";
                            System.out.println(msg);
                            System.exit(1);
                        }
                        niftiDir = Paths.get(niftiDirString);
                        iarg++;
                        break;
                    default:
                        System.out.println("Unknown argument: " + args[iarg]);
                        printUsage();
                        System.exit(1);
                }
            }

            if( projectLabel == null) {
                System.out.println("Required projectLabel not specified.");
                printUsage();
                System.exit(1);
            }
            if( subjectLabel == null) {
                System.out.println("Required subjectLabel not specified.");
                printUsage();
                System.exit(1);
            }
            if( sessionLabel == null) {
                System.out.println("Required sessionLabel not specified.");
                printUsage();
                System.exit(1);
            }
            if( sessionID == null) {
                System.out.println("SessionID not specified.");
                printUsage();
                System.exit(1);
            }
            if( sessionDir == null) {
                System.out.println("SessionDir not specified.");
                printUsage();
                System.exit(1);
            }
            if( niftiDir == null) {
                System.out.println("niftiDir not specified.");
                printUsage();
                System.exit(1);
            }

            try {
                BidsSession bs = new BidsSession( scanIdentifier, bidsNamer, new Dcm2Niix(), xnatClient);

                int exitCode = bs.process( projectLabel, subjectLabel, sessionLabel, sessionID, sessionDir, niftiDir, overwrite);
                if( exitCode == 0) {
                    System.out.println("Success.");
                }
                else {
                    System.err.println("Fail with exitCode: " + exitCode);
                }
                System.exit( exitCode);
            }
            catch (Exception e) {
                System.err.println("Exception: " + e);
                System.exit(-1);
            }
        };
    }

    private String nextArg( String[] args, int iarg) {
        return (iarg < args.length)? args[iarg]: null;
    }

    private void printUsage() {
        System.out.println("bids-session --projectLabel <projectLabel> --subjectLabel <subjectLabel> --sessionLabel <sessionLabel> --sessionID <sessionID> --overwrite <true | false> -sessionDir <path to session dir> --niftiDir <path to nifti dir>");
        System.out.println();
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication( Application.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

}
