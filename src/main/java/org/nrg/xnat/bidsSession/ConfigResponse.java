package org.nrg.xnat.bidsSession;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

@JsonRootName("ResultSet")
public class ConfigResponse {

    @JsonProperty("Result")
    private List<ConfigEntry> configEntryList;

    public List<ConfigEntry> getConfigEntryList() {
        return configEntryList;
    }

    public void setConfigEntryList(List<ConfigEntry> configEntryList) {
        this.configEntryList = configEntryList;
    }
}
