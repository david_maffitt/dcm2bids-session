package org.nrg.xnat.bidsSession;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BidsMapEntry {
    @JsonProperty("series_description")
    private String seriesDescription;
    @JsonProperty("bidsname")
    private String bidsName;

    public String getSeriesDescription() {
        return seriesDescription;
    }

    public void setSeriesDescription(String seriesDescription) {
        this.seriesDescription = seriesDescription;
    }

    public String getBidsName() {
        return bidsName;
    }

    public void setBidsName(String bidsName) {
        this.bidsName = bidsName;
    }

    @Override
    public String toString() {
        return "BidsMapEntry{" +
                "seriesDescription='" + seriesDescription + '\'' +
                ", bidsName='" + bidsName + '\'' +
                '}';
    }
}
