package org.nrg.xnat.bidsSession;

import org.nrg.xnat.bidsSession.datamodel.ImageSessionData;

import java.util.List;
import java.util.stream.Collectors;

public class AnatQCScanIdentifier implements ScanIdentifier {

    public List<BidsScanData> findScans(ImageSessionData session) {
        return session.getScans().getScan().stream()
                .filter( s -> s.getNote() != null)
                .filter( s -> s.getNote().startsWith("MOVE") || s.getNote().startsWith("ANAT"))
                .map( BidsScanData::new)
                .collect(Collectors.toList());
    }
}
