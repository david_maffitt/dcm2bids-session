package org.nrg.xnat.bidsSession;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Dcm2Niix {

    public int run(String bidsName, Path scanBidsDir, Path scanDicomDir) throws IOException, InterruptedException {
        Files.createDirectories( scanBidsDir);
        String cmdString = String.format("dcm2niix -b y -z y -f %s -o %s %s", bidsName, scanBidsDir, scanDicomDir);
        ProcessRunner processRunner = new ProcessRunner( cmdString.split(" "));

        return processRunner.run( System.out::println, System.err::println);
    }

}
