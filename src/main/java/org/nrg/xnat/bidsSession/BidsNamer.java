package org.nrg.xnat.bidsSession;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BidsNamer {
    private XNATClient xnatClient;
    private String projectLabel;
    private Map<String, String> bidsMap;

    public BidsNamer( XNATClient xnatClient) {
        this.xnatClient = xnatClient;
        this.bidsMap = new HashMap<>();
        this.projectLabel = null;
    }

    /**
     * getBidsNames
     *
     * We must compute the BIDS name of a scan in the context of the other scans in the group.
     *
     * @param scans
     * @return
     */
    public List<BidsScanData> addBidsNames( String projectLabel, String subjectLabel, String sessionLabel, List<BidsScanData> scans) {
        addSubjectLabels( subjectLabel, scans);
        appendSessionLabels( sessionLabel, scans);
        appendSeriesNames( projectLabel, scans);
        appendDuplicateRunNames( scans);
        return scans;
    }

    private Map<String, String> getBidsMap( String projectLabel) throws XNATClientException {
        Map<String, String> bidsMap = new HashMap<>();

        bidsMap.putAll( xnatClient.getSiteBidsMap());
        bidsMap.putAll( xnatClient.getProjectBidsMap( projectLabel));
        return bidsMap;
    }

    private List<BidsScanData> addSubjectLabels( String subjectLabel, List<BidsScanData> scans) {
        String bidsSubjectLabel = String.format( "sub-%s", subjectLabel);
        scans.forEach( s -> s.setBidsName( bidsSubjectLabel));
        return scans;
    }

    private List<BidsScanData> appendSessionLabels( String sessionLabel, List<BidsScanData> scans) {
        String bidsSessionLabel = String.format( "_ses-%s", sessionLabel);
        scans.forEach( s -> s.setBidsName( s.getBidsName() + bidsSessionLabel));
        return scans;
    }

    private String getDescription( BidsScanData scan) {
        String description = scan.getImageScanData().getSeriesDescription();
        description = (description == null || description.isEmpty())? scan.getImageScanData().getType(): description;
        description = (description == null || description.isEmpty())? "": description;
        return description;
    }

    private List<BidsScanData> appendSeriesNames(String projectLabel, List<BidsScanData> scans) {
        setProjectLabel( projectLabel);
        scans.forEach( s -> { String seriesName = getSeriesName(s);
                s.setSeriesName( seriesName);
                s.setBidsName( s.getBidsName() + "_" + seriesName);});
        return scans;
    }

    private String getSeriesName( BidsScanData scan) throws XNATClientException {
        String name = bidsMap.get( getDescription( scan));
        return (name == null)? "": name;
    }

    private void appendDuplicateRunNames( List<BidsScanData> scans) {
        Map<String, List<BidsScanData>> map = scans.stream()
                .collect( Collectors.groupingBy( BidsScanData::getSeriesName));
        List<List<BidsScanData>> dupScans = map.values().stream()
                .filter( lst -> lst.size() > 1).collect(Collectors.toList());
        dupScans.forEach( lst -> appendDuplicateRunName( lst));
    }

    private void appendDuplicateRunName( List<BidsScanData> scans) {
        List<BidsScanData> sortedList = scans.stream()
                .sorted( Comparator.comparing(s -> s.getImageScanData().getID()))
                .collect(Collectors.toList());
        IntStream.range( 0, sortedList.size())
                .forEach( i -> {
                    BidsScanData scan = sortedList.get(i);
                    scan.setBidsName( scan.getBidsName().concat( String.format("_run-%02d",i+1)));
                });
    }

    private void setProjectLabel(String projectLabel) {
        if( projectLabel != this.projectLabel) {
            this.projectLabel = projectLabel;
            this.bidsMap = getBidsMap( projectLabel);
        }
    }
}
