package org.nrg.xnat.bidsSession;

import org.nrg.xnat.bidsSession.datamodel.ImageSessionData;

import java.util.List;

public interface ScanIdentifier {

    List<BidsScanData> findScans(ImageSessionData session);
}
