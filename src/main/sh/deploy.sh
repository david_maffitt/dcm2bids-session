#!/bin/bash

scp bids-session deploy-local.sh ../../../target/bids-session-1.0-SNAPSHOT.jar cnda-dev:/tmp

# ssh cnda-dev 'sudo chmod u+x /tmp/deploy-local.sh'
ssh -t cnda-dev '/tmp/deploy-local.sh'