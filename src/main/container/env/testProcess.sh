#!/bin/bash

echoerr() { printf "%s\n" "$*" >&2; }
echoout() { printf "%s\n" "$*" ; }

echoout "Echo to stdout."
echoerr "Echo to stderr."
echoout "Echo to stdout."
echoerr "Echo to stderr."
echoout "Echo to stdout."
sleep 3
echoerr "Echo to stderr."
echoout "Echo to stdout."
echoerr "Echo to stderr."
echoout "Echo to stdout."
echoerr "Echo to stderr."
