#!/bin/bash

BS_PROJECT='/Users/drm/projects/nrg/dcm2bids-sess-container/bids-session'

cp ${BS_PROJECT}/src/main/sh/bids-session env
cp ${BS_PROJECT}/target/bids-session-1.0-SNAPSHOT.jar env

docker build -t registry.nrg.wustl.edu/docker/nrg-repo/bids-session:1.0 env

